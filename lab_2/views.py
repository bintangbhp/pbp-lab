from django.shortcuts import render
from datetime import datetime, date
from lab_2.models import TrackerTugas

mhs_name = 'Bintang'  
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002,3,10)  
npm = "2006462696"  


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab2.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def list_tugas(request):
    tugas = TrackerTugas.objects.all() 
    response = {'list_tugas': tugas}
    return render(request, 'deadline_tugas_lab2.html', response)
