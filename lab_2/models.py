from django.db import models
from datetime import date

# TODO Create TrackerTugas model that contains course, detail, and deadline (date) here

class TrackerTugas(models.Model):
    course = models.CharField(max_length=30)
    detail = models.CharField(max_length=255)
    deadline = models.DateField(default = date.today)