from django import forms
from lab_2.models import TrackerTugas

class TugasForm(forms.ModelForm):
    class Meta:
        model = TrackerTugas
        fields = "__all__"
        widgets = {
            'deadline' : forms.DateInput(),
            'course' : forms.TextInput(),
            'detail' : forms.Textarea(),
        }