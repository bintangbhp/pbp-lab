from django.shortcuts import render
from lab_2.models import TrackerTugas
from lab_4.forms import TugasForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url= '/admin/login/')
def index(request):
    tugas = TrackerTugas.objects.all() 
    response = {'list_tugas': tugas}
    return render(request, 'lab4_index.html', response)

@login_required(login_url= '/admin/login/')
def add_tugas(request):
    form_tugas = TugasForm(request.POST or None)
    if request.method == "POST":
        if form_tugas.is_valid():
            form_tugas.save()
    response = {'form' : form_tugas}
    return render(request, 'lab4_form.html', response)