# Lab 9: Networking and Integration

CSGE602022 - Platform-Based Programming (Pemrograman Berbasis Platform) @
Faculty of Computer Science Universitas Indonesia, Even Semester 2021/2022

---

## Tujuan Pembelajaran

Setelah menyelesaikan tutorial ini, mahasiswa diharapkan untuk mengerti :

- Mampu memahami fetching data dengan menggunakan Serializing JSON
- Mampu menampilkan hasil fetching data yang diambil dari serialization JSON ke aplikasi mobile

## Tugas

Anda diminta untuk membuat sebuah app baru di dalam project ini bernama `lab_9` yang merupakan sebuah project di atas framework flutter yang menampilkan hasil fetching data dari Serializing JSON. Pastikan di dalam tersebut sudah mengandung ListView yang menampilkan data dari serialization JSON.
Lalu, anda diminta untuk Serializing JSON lain, selain dari template yang sudah dibuat. Sebagai contoh anda dapat meng-serialize data dari API yang sudah dibuat sebelumnya atau contoh lain yang berasal dari API.

Tips: Anda dapat menggunakan tools JSON to dart untuk membuat class model(<https://app.quicktype.io/>)

## Checklist

### Running Sample Program

1. [ ] Go to directory `lab_flutter`in root directory (`pbp-lab`).
2. [ ] Run `flutter create lab_9` to initiate flutter your `lab_9` directory. You can use template from `lab_instruction/lab_9/template_flutter`. And run `flutter run` to run your app in your `lab_flutter/lab_9` directory.

### Implement my own Input page

1. [ ] Try to fetch data from JSON and display it in ListView
2. [ ] Try the application that you have built in this lab using Web Browser / Mobile Devices / Simulator, just run `flutter run`.
3. [ ] Run `flutter run` to run your app in your `lab_flutter/lab_9` directory. Serializing another JSON, as Example from <https://jsonplaceholder.typicode.com/> or previous your lab-3 and see the result.

## Referensi

1. Slide Materi: <https://docs.google.com/presentation/d/1RUp7jl1c_kCgGWMNsXUeI7ne5jc1K5R2X26x2Eht_1M/edit?usp=sharing>
2. Official Flutter Docs: <https://flutter.io/docs/>
3. <https://jsonplaceholder.typicode.com/>
4. JSON to dart class: <https://app.quicktype.io/>