# Lab 1: Introduction to Django Framework

CSGE602022 - Platform-Based Programming (Pemrograman Berbasis Platform) @
Faculty of Computer Science Universitas Indonesia, Even Semester 2021/2022

---

## Tujuan Pembelajaran

Setelah menyelesaikan tutorial ini, mahasiswa diharapkan untuk mengerti :

- Mengerti Struktur _Django Project_
- Mengerti bagaimana alur _Django_ menampilkan sebuah tulisan
- Mengerti konfigurasi routing yang ada pada _urls.py_

---

## Pengenalan Project

_Django_ merupakan salah satu _framework_ yang menggunakan bahasa pemrograman _python_.
_Framework_ sangat berguna dalam pengembangan web karena sudah menyediakan komponen-komponen
yang dibutuhkan untuk membuat dan menjalankan suatu web, tanpa harus mulai dari nol.
Sebelum memulai pengembangan web menggunakan Django, penting untuk memahami apa itu `virtual environemnt` (virtualenv).
Virtual environment (lingkungan virtual) berfungsi untuk memisahkan pengaturan dan _package_ yang di _install_  
pada setiap _Django project_ sehingga perubahan yang dilakukan pada satu _project_ tidak mempengaruhi _project_ lainnya.
Dengan kata lain, setiap _Django project_ sebaiknya memiliki _virtualenv_ nya sendiri.

> Pastikan saat pembuatan virtualenv, yang digunakan adalah python versi 3 (cek dengan `python --version` )
>
> Sebelum memulai pengembangan web, biasakan untuk selalu mengaktifkan virtualenv terlebih dahulu.

## Struktur _Django Project_

`django-admin` adalah _script_ yang digunakan untuk pembuatan _Django project_. Perintah untuk membuat suatu _project_

`django-admin startproject <NAMA-PROJECT>`

> Ganti `<NAMA-PROJECT>` dengan nama yang kalian inginkan, misalkan Lab-PBP-2021

Struktur project yang dihasilkan,

1. `virtualenv` sebagai sub-dir _project_ <NAMA-PROJECT>

```python
- <NAMA-PROJECT>
    - manage.py
    - <NAMA-PROJECT>
        __init__.py
        settings.py
        urls.py
        wsgi.py
    - <django-apps-1>
        ...
    - <django-apps-2>
        ...
    - virtualenv
        ...
```

2. `virtualenv` satu level dengan _project_ <NAMA-PROJECT>

```python
- virtualenv
    ...
- <NAMA-PROJECT>
    - manage.py
    - <NAMA-PROJECT>
        __init__.py
        asgi.py
        settings.py
        urls.py
        wsgi.py
    - <django-apps-1>
        ...
    - <django-apps-2>
        ...
```

> Direktori virtualenv bisa berada dalam direktori utama _project_ `<NAMA-PROJECT>` (sebagai sub-direktori)
> atau bisa di luar, satu level dengan direktori utama _project_ `<NAMA-PROJECT>`).
>
> Jangan lupa untuk memasukkan `virtualenv` ke dalam `.gitignore`

Perhatikan bahwa direktori dengan nama `<NAMA-PROJECT>` ada dua buah.  
Direktori yang pertama adalah direktori utama _project_ , sementara direktori yang kedua adalah
direktori konfigurasi atau pengaturan _project_ yang di dalamnya terdapat berkas `settings.py`.

`django-apps-1` dan `django-apps-2` merupakan `apps` milik Django. Untuk membuat suatu app, gunakan perintah

`python manage.py startapp <app-name>`

> ganti`<app-name>` menggunakan nama sesuai kebutuhan/keinginan kalian, misalkan `lab_x`.
> sebelum menjalankan perintah ini, pastikan sudah berada satu direktori dengan berkas `manage.py`.
>
> Coba perintah `ls` (linux) atau `dir` (windows)

Struktur umum dari suatu `apps` ialah :

```python
    - <app-name>
        __init__.py
        admin.py
        apps.py
        models.py
        tests.py
        views.py
        - migrations
            ...
        - templates
            ...
```

> Suatu `app` dianggap aktif atau terpakai jika `app` tersebut didaftarkan di pengaturan `INSTALLED_APPS`
> pada berkas `settings.py` (ada pada direktori pengaturan _project_, yang nama direktorinya sama dengan nama _project_ Django)

Secara _default_ , tidak ada berkas `urls.py` karena Django memberikan kebebasan untuk membuat _routing_ sesuai kebutuhan pengembang.
Namun untuk _best practice_ dan kemudahan pengembangan, berkas `urls.py` dibuat manual untuk setiap `app`. Berkas `urls.py` satu level
(satu direktori) dengan berkas `views.py`

Selain itu, untuk menyimpan berkas `HTML` (misalkan `index.html`) biasanya dibuat suatu direktori bernama `templates` di
dalam direktori `<app-name>`, jadi struktur dari suatu `app` nantinya akan menjadi

```python
    - <app-name>
        __init__.py
        admin.py
        apps.py
        models.py
        tests.py
        views.py
        urls.py
        - migrations
            ...
        - templates
            ...
```

> Apa beda `Project` dan `App` ? _Project_ adalah kumpulan konfigurasi dan beberapa app (aplikasi) untuk suatu website tertentu.
> Sedangkan _App_ adalah suatu aplikasi web yang memiliki fungsi/tugas tertentu, misalkan sebagai database atau sebagai aplikasi survei sederhana.
> Satu _project_ dapat memiliki banyak _app_, dan satu _app_ dapat digunakan di banyak _project_.
>
> Tutorial pembuatan Django _project_ : <cite> <https://docs.djangoproject.com/en/3.2/intro/tutorial01/> </cite>

Sekilas tentang berkas `settings.py` , pada berkas ini terdapat _section_ `INSTALLED_APPS` yang berfungsi untuk
mendaftarkan aplikasi yang akan dipakai/dijalankan pada suatu _project_.
Contohnya, mendaftarkan `app` bernama "lab_pbp" ke INSTALLED_APPS :

```python
INSTALLED_APPS = [
    ...
    lab_1,
    lab_pbp,
]
```

> jika "sedang" tidak ingin menggunakan suatu `app`, daripada menghapus folder `app` tersebut, kamu bisa
> menon-aktifkan `app` tersebut dengan menghapusnya dari _INSTALLED_APPS_

## Routing pada Django

Routing dapat diumpamakan sebagai suatu pemetaan. URL (Uniform Resource Locator) atau sederhananya adalah suatu alamat web.
`http://localhost:8000` merupakan contoh sederhana dari suatu URL. `http://localhost` adalah alamat utamanya, sedangkan `8000`
adalah _port_ yang digunakan. Django memiliki python _module_ bernama `URLconf` (URL configuration) berisi sekumpulan
pola atau _pattern_ yang Django akan coba cocokkan untuk menemukan `views` (tampilan) yang benar.

> Django menggunakan _regular expression_ atau _regex_ untuk melakukan pencocokan antara URL dengan `views` (tampilan).
> Kalau penasaran bagaimana membuat _regex_ pada Python , coba cek [link](https://docs.python.org/3/howto/regex.html) berikut

Perhatikan struktur _project_ Django, terdapat sub-dir dengan nama sama persis dengan nama project yang dibuat, dengan struktur :

```python
<NAMA-PROJECT>
    __init__.py
    asgi.py
    settings.py
    urls.py
    wsgi.py
```

Secara sederhana, format penulisan utk URL pada Django ialah `url(regex, view, kwargs=None, name=None)`

- regex ialah _pattern_ yang akan dicocokkan
- `view` ialah fungsi yang untuk memproses _request_ dan mengatur tampilan.
- _kwargs_ dan _name_ saat ini bisa diabaikan/dikosongkan

> Untuk mengetahui lebih lanjut format penulisan urls, cek [link] (<https://docs.djangoproject.com/en/3.2/_modules/django/conf/urls/#url>) berikut

Berkas `urls.py` pada direktori ini adalah contoh URLconf yang disediakan oleh Django, yang dapat digunakan untuk
melakukan `routing` ke `apps` Django lainnya. Contoh untuk membuat `routing` ke `app` lain , nantinya pada `lab_2` kalian akan routing dengan module/app:

Penjelasan ringkas :

> INGAT: penulisan variabel, parameter, fungs, dsb, pada Django _case-sensitif_. Jadi harus teliti dalam menulisakan kode
>
> Penjelasan ringkas mengenai URL Django : <cite> <https://tutorial.djangogirls.org/en/django_urls/> </cite>
>
> Penjelasan lebih detail bagaimana URLconf bekerja : <cite> <https://docs.djangoproject.com/en/3.2/topics/http/urls/> </cite>

## Cara Menampilkan _Webpage_

Pada lab ini anda telah disediakan sebuah _template_ `apps` Django. Tugas Anda adalah melengkapi _urls.py_ yang sudah diberikan sesuai dengan yang sudah diajarkan pada sesi kelas berkaitan dengan _routing_ pada framework Django.

1. Bukalah folder `praktikum` lalu  dalam `urls.py`.

2. Lengkapi TODOs pada yang diberikan. Modifikasi `urls.py` agar pada saat _request_ diberikan pada `<HOSTNAME>` , `<HOSTNAME>/welcome/<str:name>`,dan `<HOSTNAME>/aboutme/<str:name>/<str:hobby>` bisa dimunculkan.

3. Jalankan Django secara lokal :

    > python manage.py runserver

    atau

    > python manage.py runserver 8000

    Secara _default_, perintah `runserver` dengan _port_ `8000`, tentu kalian dapat mengubah _port_ tersebut. Sebagai contoh  dibawah ini menggunakan _port_ `8080`:

    > python manage.py runserver 8080

4. Selamat, kalian sudah berhasil menampilkan webpage sebagai tugas lab_1 ini.

5. `commit` dan `push` pekerjaan kalian ke repo masing - masing

## Checklist

1. [ ] Create Your own Gitlab Repo. If you did the previous Tutorial (Initial Setup and Doing the Tutorial), then you're good to go.

2. [ ] Create virtual environment and make sure you can run your Django project (see Tutorial: Running Your Django Project)

3. [ ] Create an admin user (see: <https://docs.djangoproject.com/en/1.8/intro/tutorial02/>)

4. Display your profile page:

   1. [ ] Implement TODOs on urls.py
   2. [ ] Refresh <http://localhost:8000/> and try another port server
   3. [ ] Try routing `<HOSTNAME>`, `<HOSTNAME>/welcome/<str:name>` and `<HOSTNAME>/aboutme/<str:name>/<str:hobby>`
