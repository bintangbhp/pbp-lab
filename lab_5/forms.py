from django import forms
from lab_5.models import MyWatchlist

class FilmForms(forms.ModelForm):
    class Meta:
        model = MyWatchlist
        fields = "__all__"
        # widgets = {
        #     'watched' : forms.CheckboxInput(attrs={'id':MyWatchlist.watched}),
        #     'title' : forms.TextInput(attrs={'id':MyWatchlist.title}),
        #     'rating' : forms.NumberInput(attrs={'id':MyWatchlist.rating,'step':'0.1'}),
        #     'release_date' : forms.DateInput(attrs={'id':MyWatchlist.release_date}),
        #     'review' : forms.Textarea(attrs={'id':MyWatchlist.review}),
        #     # 'image_content' : forms.FileInput(attrs={'id':MyWatchlist.image_content}),
        # }