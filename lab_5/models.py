from datetime import date
from django.db import models

# Create your models here.
class MyWatchlist(models.Model):
    watched = models.BooleanField(default=False)
    title = models.CharField(max_length=100)
    rating = models.FloatField()
    release_date = models.DateField(default= date.today)
    review = models.TextField()
    image_content = models.ImageField(upload_to='lab5-images')