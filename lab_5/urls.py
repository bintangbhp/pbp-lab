from django.urls import path
from lab_5.views import index, watchlist, add_film


urlpatterns = [
    path('', index),
    path('watchlist', watchlist),
    path('add', add_film),
]

