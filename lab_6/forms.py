from django import forms
from lab_5.models import MyWatchlist

class FilmForms(forms.ModelForm):
    class Meta:
        model = MyWatchlist
        fields = "__all__"