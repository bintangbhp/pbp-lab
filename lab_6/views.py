from django.shortcuts import render
from lab_5.models import MyWatchlist
from django.http.response import HttpResponse

# Create your views here.

def index(request):
    film = MyWatchlist.objects.all() 
    response = {'list_film': film}
    return render(request, 'lab6_index.html', response)
