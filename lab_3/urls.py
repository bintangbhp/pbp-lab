from django.urls import path
from .views import list_film, list_film_json, list_film_xml

urlpatterns = [
    path('', list_film),
    path('xml', list_film_xml),
    path('json', list_film_json),
]
