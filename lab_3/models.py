from sys import maxsize
from django.db import models

# Create your models here.

class MyWatchlist(models.Model):
    watched = models.BooleanField(default=False)
    title = models.CharField(max_length=50)
    rating = models.FloatField(default=0.0)
    release_date = models.CharField(max_length=30)
    review = models.CharField(max_length=255)