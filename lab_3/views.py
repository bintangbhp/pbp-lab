from django.shortcuts import render
from lab_3.models import MyWatchlist
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.

def list_film(request):
    film = MyWatchlist.objects.all() 
    response = {'list_film': film}
    return render(request, 'lab3.html', response)

def list_film_xml(request):
    film = MyWatchlist.objects.all()
    data = serializers.serialize('xml', film)
    return HttpResponse(data, content_type="application/xml")

def list_film_json(request):
    film = MyWatchlist.objects.all()
    data = serializers.serialize('json', film)
    return HttpResponse(data, content_type="application/json")