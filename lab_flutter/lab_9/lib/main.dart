import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lab_9/api_model/film_model.dart';

Future<List<Film>> fetchFilm() async {
  String url = "http://127.0.0.1:8000/lab-3/json/";
  http.Response response = await http.get(Uri.parse(url));
  var data = jsonDecode(utf8.decode(response.bodyBytes));
  List<Film> film = [];
  for (var d in data) {
    if (d != null) {
      film.add(Film.fromJson(d));
    }
  }
  return film;
}

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Future<List<Film>> futureFilm;

  @override
  void initState() {
    super.initState();
    futureFilm = fetchFilm();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Daftar Film',
      theme: ThemeData(
        primaryColor: Colors.lightBlueAccent,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Daftar Film'),
        ),
        body: FutureBuilder<List<Film>>(
          future: futureFilm,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                physics: const ClampingScrollPhysics(),
                itemCount: snapshot.data!.length,
                itemBuilder: (_, index) => Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  padding: const EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 179, 172, 41),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        snapshot.data![index].fields.title,
                        style: const TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 10),
                      Text("Watched: " +
                          (snapshot.data![index].fields.watched == true
                              ? "Yes"
                              : "No")),
                      Text("Release Date: " +
                          snapshot.data![index].fields.releaseDate),
                      Text("Rating: " +
                          snapshot.data![index].fields.rating.toString()),
                      Text("Review: " + snapshot.data![index].fields.review),
                    ],
                  ),
                ),
              );
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }
}
